from django.contrib import admin
from .models import Skills

class PersonaAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at',)
    search_fields = ('Bueno','Malo')
    list_display = ('Bueno','Malo', 'created_at')
    ordering = ('-created_at',)

    def save_model(self, request, obj, form, change):
        if not obj.user_id:
            obj.user_id = request.user.id
        obj.save()

admin.site.register(Skills, PersonaAdmin)
# Register your models here.
